public class App {
    public static void main(String[] args) throws Exception {
        
        //Construir un avión de carga
        System.out.println("------AVION DE CARGA------");
        AvionCarga objAvionCarga = new AvionCarga("Gris",150.5);
        objAvionCarga.cargar();        
        System.out.println(objAvionCarga.despegar());
        objAvionCarga.aterrizar();
        objAvionCarga.descargar();
        

        //Cosntruir avión de pasajeros
        System.out.println("------AVION DE PASAJEROS------");
        AvionPasajeros objAvionPasajeros = new AvionPasajeros("Gris", 112.5, 6);
        objAvionPasajeros.despegar();
        objAvionPasajeros.servir();
        objAvionPasajeros.aterrizar();
        objAvionPasajeros.frenar();
        

        System.out.println("--------AVION MILITAR--------");
        AvionMilitar objAvionMilitar = new AvionMilitar("Verde", 181.12,500);
        objAvionMilitar.despegar();
        //objAvionMilitar.disparar(); //no se puede disparar porque es privado, esta encapsulado y solo se puede llamar desde la misma clase
        objAvionMilitar.aterrizar();
        objAvionMilitar.frenar();
       
        
    }
}
