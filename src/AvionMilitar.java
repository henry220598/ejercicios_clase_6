public class AvionMilitar extends Avion{

    /*********
     * CONSTRUCTOR PARA CONECTAR CON AVION
     */
    public AvionMilitar(String color, double tamano, int misiles){
        super(color, tamano);
    }
    /**
     * Atributos
     */
    private int misiles;

    /**
     * Métodos
     */
    private void disparar(){
        System.out.println("Disparando...");
    }
}